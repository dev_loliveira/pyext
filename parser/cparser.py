#!/usr/bin/python
import os

from copy import copy
from parser import BaseParser
from subprocess import Popen, PIPE


class CParser(BaseParser):
    BASE_NORM_CMD = ['gcc', '-E', '-std=c99']

    def __init__(self, *args, **kwargs):
        super(CParser, self).__init__(*args, **kwargs)
        self.methods = []

    def visit_FuncDef(self, node):
        self.methods.append(
            (node.decl.name, 'Declared at %s' % node.decl.coord)
        )

    def fetch_functiondefs(self):
        self.visit(self.parse())
        return self.methods

    def is_valid(self):
        return True if self.filepath.split('.')[-1] == 'c' else False


    def normalize(self):
        base_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), os.path.pardir))
        fake_libs = os.path.join(base_dir, 'pycparser', 'utils', 'fake_libc_include')
        cmd = copy(CParser.BASE_NORM_CMD)
        cmd.append('-I%s' % fake_libs)
        cmd.append(self.filepath)
        process = Popen(cmd, stdout=PIPE)

        return process.stdout.read()
