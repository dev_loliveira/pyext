from tempfile import mkstemp
from pycparser import c_parser, c_ast, parse_file


class BaseParser(c_ast.NodeVisitor):

    def __init__(self, filepath):
        self.filepath = filepath

    def parse(self, use_cpp=True):
        return parse_file(self.fetch_normalized_file(), use_cpp)

    def visit_FuncDef(self, node):
        raise NotImplementedError

    def is_valid(self):
        raise NotImplementedError('')

    def fetch_functiondefs(self):
        raise NotImplementedError('')

    def normalize(self):
        raise NotImplementedError('')

    def fetch_normalized_file(self):
        tmphandle = open(mkstemp()[1], 'w')
        with open(self.filepath) as original_file:
            contents = original_file.read()
        normalized_data = self.normalize()

        tmphandle.write(normalized_data)
        tmphandle.close()

        return tmphandle.name
