#!/usr/bin/python
import sys
from factory.wrapper import WrapperFactory
from factory.setup import SetupFactory
from parser.cparser import CParser



if __name__ == '__main__':
    src_name = sys.argv[1]
    module_docstring = 'Module Hello world!'
    cparser = CParser(src_name)
    methods_tuple = cparser.fetch_functiondefs()

    wrapper = WrapperFactory(src_name, module_docstring, methods_tuple)
    setup = SetupFactory(src_name, module_docstring, methods_tuple)
    wrapper.generate()
    setup.generate()
