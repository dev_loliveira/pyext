
import os
from factory import BaseFactory


class SetupFactory(BaseFactory):
    TEMPLATE_PATH = os.path.join( os.path.dirname(__file__), 'templates', 'setup.template' )

    def generate(self):
        base_template = self.get_template_contents()
        template = base_template.format(self.src_name, self.src_name_ext)

        self.write_to_disk(template)

    def get_template_contents(self):
        with open(SetupFactory.TEMPLATE_PATH) as fhandle:
            return fhandle.read()

    def output_filepath(self):
        return 'setup.py'
