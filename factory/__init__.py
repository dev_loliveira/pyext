import os


class BaseFactory(object):

    def __init__(self, src_name, module_docstring, methods_tuple):
        self.src_name = os.path.basename(src_name).split('.')[0]
        self.src_name_ext = os.path.basename(src_name)
        self.module_docstring = module_docstring
        self.methods_tuple = methods_tuple

    def write_to_disk(self, contents):
        with open(self.output_filepath(), 'w') as fhandle:
            fhandle.write(contents)

    def output_filepath(self):
        raise NotImplementedError('')

    def get_template_contents(self):
        raise NotImplementedError('')
