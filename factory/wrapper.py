
import os
from factory import BaseFactory


class WrapperFactory(BaseFactory):
    TEMPLATE_PATH = os.path.join( os.path.dirname(os.path.abspath(__file__)), 'templates', 'wrapper.template' )

    def generate(self):
        methods_docstring = self.extract_methods_data(self.get_methods_docstring)
        methods_declaration = self.extract_methods_data(self.get_methods_declaration)
        module_methods = self.extract_methods_data(self.get_module_methods)
        methods_implementation = self.extract_methods_data(self.get_methods_implementation)

        base_template = self.get_template_contents()
        template = base_template % (
            self.src_name, methods_docstring, methods_declaration,
            module_methods, self.src_name, self.src_name, methods_implementation
        )

        self.write_to_disk(template)

    def extract_methods_data(self, function_handle):
        result = []
        for method_tuple in self.methods_tuple:
            result.append(function_handle(method_tuple))

        return '\n'.join(result)

    def get_methods_docstring(self, method_tuple):
       name, docstring = method_tuple
       docstring = 'static char %s_docstring [] = "%s";' % (
           name, docstring
       )

       return docstring

    def get_methods_declaration(self, method_tuple):
        name, docstring = method_tuple
        declaration = 'static PyObject *%s_%s(PyObject *self, PyObject *args);' % (
            self.src_name, name
        )

        return declaration

    def get_module_methods(self, method_tuple):
        name, docstring = method_tuple
        meth_str = '    {"%s", %s_%s, METH_VARARGS, %s_docstring},' % (
            name, self.src_name, name, name
        )

        return meth_str

    def get_methods_implementation(self, method_tuple):
        name, docstring = method_tuple
        implementation = 'static PyObject *%s_%s(PyObject *self, PyObject *args) {\n}\n' % (
            self.src_name, name
        )

        return implementation


    def output_filepath(self):
        return '_%s' % self.src_name_ext

    def get_template_contents(self):
        with open(WrapperFactory.TEMPLATE_PATH) as template_handle:
            return template_handle.read()
