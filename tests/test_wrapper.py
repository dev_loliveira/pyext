
import unittest
from factory.wrapper import WrapperFactory


class WrapperFactoryTest(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(WrapperFactoryTest, self).__init__(*args, **kwargs)
        self.wrapper = WrapperFactory('mymodule.c', 'module docstring', (
            ('methodA', 'methodA docstring'),
            ('methodB', 'methodB docstring'),
        ))

    def test_get_methods_docstring_should_return_the_correct_value(self):
        expectedA = 'static char methodA_docstring [] = "methodA docstring";'
        expectedB = 'static char methodB_docstring [] = "methodB docstring";'
        returned = self.wrapper.extract_methods_data(self.wrapper.get_methods_docstring)

        self.assertEquals(expectedA, returned.split('\n')[0])
        self.assertEquals(expectedB, returned.split('\n')[1])


    def test_get_methods_declaration_should_return_the_correct_value(self):
        expectedA = 'static PyObject *mymodule_methodA(PyObject *self, PyObject *args);'
        expectedB = 'static PyObject *mymodule_methodB(PyObject *self, PyObject *args);'
        returned = self.wrapper.extract_methods_data(self.wrapper.get_methods_declaration)

        self.assertEquals(expectedA, returned.split('\n')[0])
        self.assertEquals(expectedB, returned.split('\n')[1])

    def test_get_module_methods_should_return_the_correct_value(self):
        expectedA = '{"methodA", mymodule_methodA, METH_VARARGS, methodA_docstring},'
        expectedB = '{"methodB", mymodule_methodB, METH_VARARGS, methodB_docstring},'
        returned = self.wrapper.extract_methods_data(self.wrapper.get_module_methods)

        self.assertEquals(expectedA, returned.split('\n')[0])
        self.assertEquals(expectedB, returned.split('\n')[1])

    def test_get_methods_implementation_should_return_the_correct_value(self):
        expectedA = 'static PyObject *mymodule_methodA(PyObject *self, PyObject *args) { }'
        expectedB = 'static PyObject *mymodule_methodB(PyObject *self, PyObject *args) { }'
        returned = self.wrapper.extract_methods_data(self.wrapper.get_methods_implementation)

        self.assertEquals(expectedA, returned.split('\n')[0])
        self.assertEquals(expectedB, returned.split('\n')[1])

    def test_outputpath_should_return_the_correct_value(self):
        self.assertEquals('_mymodule.c', self.wrapper.output_filepath())
